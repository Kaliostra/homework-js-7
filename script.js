"use strict";
// 1. Это объектная модель документа,с помощью которой можно работать с HTML.CSS  через JS и изменять их значение и классы.
// 2.InnerText может только создавать свойства в виде текста для чтения так сказать безопасная запись, а InnerHtml передает такжет и элементы и узлы.
// 3. getElement и querySelector. Чаще всего работают со вторым методом так как там можна обратиться сразу и к классу и к id и элементу и нам вернется статическая колекция с которой мы можем дальше работать. А первый метод требует выбор определенного поиска и вернет нам живую колекцию которая дает нам более актуальные данные во время ее вызова.

// 1.
const paragraphText = document.querySelectorAll("p");
console.log(paragraphText);
for (const paragraphColor of paragraphText) {
    paragraphColor.style.backgroundColor = "#ff0000";
}
// // 2.
const idElement = document.getElementById("optionsList");
console.log(idElement);

const parentElement = idElement.parentElement;
console.log(parentElement);

const childElement = parentElement.children;
console.log(childElement);
// // 3.
const testParagraph = document.createElement("p");
console.log(testParagraph);
testParagraph.innerHTML = "Это параграф";
idElement.append(testParagraph);
// 4.
const elem = document.querySelectorAll(".main-header .main-nav-item");
console.log(elem);
for (const result of elem) {
    result.classList.replace("main-nav-item", "nav-item");
}
console.log(elem);
// 5.
const sectionTitle = document.getElementsByClassName("section-title");
console.log(sectionTitle);
for (const iterator of sectionTitle) {
    iterator.classList.remove("section-title");
}
console.log(sectionTitle);
